<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::group(['middleware' => ['auth']], function() {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('teams', 'TeamsController');
    Route::resource('team_players', 'TeamPlayersController');

    //need these after search
    Route::post('/team_players', 'TeamPlayersController@index')->name('team_players.search');
    Route::post('/team_players/store', 'TeamPlayersController@store')->name('team_players.store');

    // routes for matches
    Route::resource('cricket_matches', 'CricketMatchesController');

    Route::get('/points', 'CricketMatchesController@points')->name('team.points');
});
