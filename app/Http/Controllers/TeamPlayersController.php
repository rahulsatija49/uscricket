<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\TeamPlayer;
use Illuminate\Http\Request;
use Exception;

class TeamPlayersController extends Controller
{

    /**
     * Display a listing of the team players.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        $uuid = $request->input('uuid');
        $team_id = $request->input('team_id');
        if ($request->isMethod('post')) {
          $teamPlayers = TeamPlayer::where('team_id', $team_id)->paginate($this->pageSize);
        }else{
          $teamPlayers = TeamPlayer::with('team')->paginate($this->pageSize);
        }
        $teams = Team::pluck('team_name','id')->all();
        // $teams = Team::where('is_active',1)->pluck('team_name','id');
        return view('team_players.index', compact('teamPlayers','teams','uuid','team_id'));
    }

    /**
     * Show the form for creating a new team player.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $teams = Team::pluck('team_name','id')->all();

        return view('team_players.create', compact('teams'));
    }

    /**
     * Store a new team player in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            $file = $request->file('imageUri');
            if(isset($file)){

              //File Details
              $filename = $file->getClientOriginalName();
              $extension = $file->getClientOriginalExtension();
              $tempPath = $file->getRealPath();
              $fileSize = $file->getSize();
              $mimeType = $file->getMimeType();
            }else{
              $extension= $filename= '';
            }
            $valid_extension = array("jpeg","png","jpg");
            $data = $this->getData($request);
            $data['imageUri'] = $filename;
            TeamPlayer::create($data);
            // Check file extension
             if(in_array(strtolower($extension),$valid_extension)){
               // File upload location
               $location = 'uploads/team_players';
               $file->move(public_path($location), $filename);
             }

            return redirect()->route('team_players.index')
                ->with('success_message', 'Team Player was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified team player.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $teamPlayer = TeamPlayer::with('team')->findOrFail($id);

        return view('team_players.show', compact('teamPlayer'));
    }

    /**
     * Show the form for editing the specified team player.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $teamPlayer = TeamPlayer::findOrFail($id);
        $teams = Team::pluck('team_name','id')->all();

        return view('team_players.edit', compact('teamPlayer','teams'));
    }

    /**
     * Update the specified team player in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            $file = $request->file('imageUri');
            if(isset($file)){

              //File Details
              $filename = $file->getClientOriginalName();
              $extension = $file->getClientOriginalExtension();
              $tempPath = $file->getRealPath();
              $fileSize = $file->getSize();
              $mimeType = $file->getMimeType();
            }else{
              $extension= $filename= '';
            }
            $valid_extension = array("jpeg","png","jpg");
            $data = $this->getData($request);
            if($filename!=''){
              $data['imageUri'] = $filename;
            }
            $teamPlayer = TeamPlayer::findOrFail($id);
            $teamPlayer->update($data);
            // Check file extension
             if(in_array(strtolower($extension),$valid_extension)){
               // File upload location
               $location = 'uploads/team_players';
               $file->move(public_path($location), $filename);
             }
            return redirect()->route('team_players.index')
                ->with('success_message', 'Team Player was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified team player from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $teamPlayer = TeamPlayer::findOrFail($id);
            $teamPlayer->delete();

            return redirect()->route('team_players.index')
                ->with('success_message', 'Team Player was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'team_id' => 'nullable',
            'first_name' => 'required|string|min:1|nullable',
            'last_name' => 'required|string|min:1|nullable',
            'imageUri' =>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //'imageUri' => 'string|min:1|nullable',
            'player_jerseyno' => 'numeric|min:1|nullable',
            //'player_jerseyno' => 'string|min:1|nullable',
            'dob' => 'string|min:1|nullable',
            'country' => 'string|min:1|nullable',
            'matches' => 'numeric|min:1|nullable',
            'run' => 'numeric|min:1|nullable',
            'highest_scores' => 'numeric|min:1|nullable',
            'fifties' => 'numeric|min:1|nullable',
            'hundreds' => 'numeric|min:1|nullable',
            'player_type' => 'required|string|min:1|nullable',

        ];


        $data = $request->validate($rules);


        $data['is_active'] = $request->has('is_active');


        return $data;
    }

}
