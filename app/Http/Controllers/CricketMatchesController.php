<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\CricketMatches;
use App\Models\Team;
use Illuminate\Http\Request;
use Exception;

class CricketMatchesController extends Controller
{

    /**
     * Display a listing of the cricket matches.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $cricketMatchesObjects = CricketMatches::paginate($this->pageSize);
        return view('cricket_matches.index', compact('cricketMatchesObjects'));
    }

    /**
     * Display a points table of teams.
     *
     * @return Illuminate\View\View
     */
    public function points()
    {
        $teams = Team::pluck('team_name','id')->all();
        $team_data=array();
        foreach($teams as $key=> $team){
          // $key;
           $team_data[$key]['name']=$team;
           //$teams[]['matches']='2';
           $team_data[$key]['team_matches'] =  CricketMatches::where('team_one','=',$key)->orwhere('team_two','=',$key)->count();

           $team_data[$key]['team_no_result'] =  CricketMatches::where('team_one','=',$key)->where('match_result','=','')->orwhere('team_two','=',$key)->where('match_result','=','')->count();

           $team_data[$key]['team_win_count'] = CricketMatches::where('match_result','=',$key)->count();
           $team_data[$key]['team_loose_count'] = $team_data[$key]['team_matches'] - $team_data[$key]['team_win_count']-$team_data[$key]['team_no_result'];
        }
        // echo '<pre>';
        // print_r($team_data);
        // echo '</pre>';
        // echo '<br>';
        // die;
        return view('cricket_matches.matchPoints', compact('team_data'));
    }

    /**
     * Show the form for creating a new cricket matches.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $teams = Team::pluck('team_name','id')->all();
        return view('cricket_matches.create', compact('teams'));
    }

    /**
     * Store a new cricket matches in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {

            $data = $this->getData($request);
            $cricketMatches= CricketMatches::create($data);
            return redirect()->route('cricket_matches.edit',$cricketMatches->id )
                ->with('success_message', 'Cricket Matches was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified cricket matches.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $cricketMatches = CricketMatches::findOrFail($id);

        return view('cricket_matches.show', compact('cricketMatches'));
    }

    /**
     * Show the form for editing the specified cricket matches.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $cricketMatches = CricketMatches::findOrFail($id);
        $teams = Team::pluck('team_name','id')->all();
        return view('cricket_matches.edit', compact('cricketMatches','teams'));
    }

    /**
     * Update the specified cricket matches in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {

            $data = $this->getData($request);

            $cricketMatches = CricketMatches::findOrFail($id);
            $cricketMatches->update($data);

            return redirect()->route('cricket_matches.index')
                ->with('success_message', 'Cricket Matches was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request. Check Both teams are not same.']);
        }
    }

    /**
     * Remove the specified cricket matches from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $cricketMatches = CricketMatches::findOrFail($id);
            $cricketMatches->delete();

            return redirect()->route('cricket_matches.index')
                ->with('success_message', 'Cricket Matches was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request. Check Both teams are not same.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'team_one' => 'string|min:1|nullable',
            'team_two' => 'string|min:1|nullable|different:team_one',
            'match_date' => 'string|min:1|nullable',
            'match_venue' => 'string|min:1|nullable',
            'match_result' => 'string|min:1|nullable',
        ];


        $data = $request->validate($rules);




        return $data;
    }

}
