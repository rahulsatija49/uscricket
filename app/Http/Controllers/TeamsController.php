<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\TeamPlayer;
use Illuminate\Http\Request;
use Exception;

class TeamsController extends Controller
{

    /**
     * Display a listing of the teams.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $teams = Team::paginate($this->pageSize);

        return view('teams.index', compact('teams'));
    }

    /**
     * Show the form for creating a new team.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {


        return view('teams.create');
    }

    /**
     * Store a new team in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
          'team_name' => 'required|string|min:1|nullable|unique:teams,team_name',
        ]);
        try {
            $file = $request->file('logoUri');
            if(isset($file)){

              //File Details
              $filename = $file->getClientOriginalName();
              $extension = $file->getClientOriginalExtension();
              $tempPath = $file->getRealPath();
              $fileSize = $file->getSize();
              $mimeType = $file->getMimeType();
            }else{
              $extension= $filename= '';
            }
            $valid_extension = array("jpeg","png","jpg");
            $data = $this->getData($request);
            $data['logoUri'] = $filename;
            Team::create($data);
            // Check file extension
             if(in_array(strtolower($extension),$valid_extension)){
               // File upload location
               $location = 'uploads/team_logo';
               $file->move(public_path($location), $filename);
             }
            return redirect()->route('teams.index')
                ->with('success_message', 'Team was successfully added. Please select Match Winner');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified team.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $team = Team::findOrFail($id);
        $teamPlayers = TeamPlayer::where('team_id', $id)->paginate($this->pageSize);
        return view('teams.show', compact('team','teamPlayers'));
    }

    /**
     * Show the form for editing the specified team.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $team = Team::findOrFail($id);
        return view('teams.edit', compact('team'));
    }

    /**
     * Update the specified team in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $request->validate([
          'team_name' => "required|string|min:1|nullable|unique:teams,team_name,$id,id",
        ]);

        try {
            $file = $request->file('logoUri');
            if(isset($file)){
              //File Details
              $filename = $file->getClientOriginalName();
              $extension = $file->getClientOriginalExtension();
              $tempPath = $file->getRealPath();
              $fileSize = $file->getSize();
              $mimeType = $file->getMimeType();
            }else{
              $extension= $filename= '';
            }
            $valid_extension = array("jpeg","png","jpg");
            $data = $this->getData($request);
            if($filename!=''){
              $data['logoUri'] = $filename;
            }
            $team = Team::findOrFail($id);
            $team->update($data);
            // Check file extension
             if(in_array(strtolower($extension),$valid_extension)){
               // File upload location
               $location = 'uploads/team_logo';
               $file->move(public_path($location), $filename);
             }
            return redirect()->route('teams.index')
                ->with('success_message', 'Team was successfully updated.');
        } catch (Exception $exception) {
            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified team from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $team = Team::findOrFail($id);
            $team->delete();

            return redirect()->route('teams.index')
                ->with('success_message', 'Team was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'team_name' => 'required|string|min:1|nullable',
            //'logoUri' => 'string|min:1|nullable',
            'logoUri' =>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'team_clubstate' => 'required|string|min:1|nullable',

        ];


        $data = $request->validate($rules);


        $data['is_active'] = $request->has('is_active');


        return $data;
    }

}
