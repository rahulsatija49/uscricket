<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CricketMatches extends Model
{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cricket_matches';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'team_one',
                  'team_two',
                  'match_date',
                  'match_venue',
                  'match_result'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];


    /**
     * Set the match_date.
     *
     * @param  string  $value
     * @return void
     */
    // public function setMatchDateAttribute($value)
    // {
    //     $this->attributes['match_date'] = !empty($value) ? \DateTime::createFromFormat('[% date_format %]', $value) : null;
    // }

    /**
     * Get match_date in array format
     *
     * @param  string  $value
     * @return array
     */
    // public function getMatchDateAttribute($value)
    // {
    //     return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('j/n/Y');
    // }

    public function team_a()
    {
        return $this->belongsTo('App\Models\Team','team_one');
    }
    public function team_b()
    {
        return $this->belongsTo('App\Models\Team','team_two');
    }
    public function team_winner()
    {
        return $this->belongsTo('App\Models\Team','match_result');
    }

}
