<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamPlayer extends Model
{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'team_players';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'team_id',
                  'first_name',
                  'last_name',
                  'imageUri',
                  'player_jerseyno',
                  'dob',
                  'country',
                  'matches',
                  'run',
                  'highest_scores',
                  'fifties',
                  'hundreds',
                  'player_type'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the team for this model.
     *
     * @return App\Models\Team
     */
    public function team()
    {
        return $this->belongsTo('App\Models\Team','team_id');
    }



}
