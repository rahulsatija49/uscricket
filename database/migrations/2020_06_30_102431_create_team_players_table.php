<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_players', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('team_id')->unsigned()->nullable()->index();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('imageUri')->nullable();
            $table->string('player_jerseyno')->nullable();
            $table->string('dob')->nullable();
            $table->string('country')->nullable();
            $table->string('matches')->nullable();
            $table->string('run')->nullable();
            $table->string('highest_scores')->nullable();
            $table->string('fifties')->nullable();
            $table->string('hundreds')->nullable();
            $table->string('player_type')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('team_players');
    }
}
