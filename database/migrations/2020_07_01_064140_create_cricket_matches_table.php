<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCricketMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cricket_matches', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('team_one')->nullable();
            $table->string('team_two')->nullable();
            $table->string('match_date')->nullable();
            $table->string('match_venue')->nullable();
            $table->string('match_result')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cricket_matches');
    }
}
