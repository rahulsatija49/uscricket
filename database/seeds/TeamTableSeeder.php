<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //
      $faker = Faker\Factory::create();
      DB::table('teams')->insert([
                [
                  'team_name' => "India",
                  'team_clubstate' => "Delhi",
                  'created_at' => $faker->dateTime(),
                  'updated_at' => $faker->dateTime()
                ],
                [
                'team_name' => "Australia",
                'team_clubstate' => "Perth",
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime()
                ]
      ]);
    }
}
