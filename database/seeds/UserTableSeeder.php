<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        DB::table('users')->insert([
                    [
                    'name' => "Rahul",
                    'email' => "rahulsatija49@gmail.com",
                    'password' => bcrypt('rahulsatija49'),
                    'created_at' => $faker->dateTime(),
                    'updated_at' => $faker->dateTime()
                    ]
        ]);
    }
}
