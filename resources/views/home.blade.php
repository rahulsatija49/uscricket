@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><h4>{{__('Menu')}}</h4></div>
                <div class="panel-body">
                    <p class="text-center">
                       <br><br>
                       <a style="min-width:250px;" class="btn btn-success" href="{{ route('teams.index')}}">{{__('Teams List')}}</a> <br><br>
                       <a style="min-width:250px;" class="btn btn-success" href="{{ route('team_players.index') }}">{{__('Teams Players List')}}</a><br><br>
                       <a style="min-width:250px;" class="btn btn-success" href="{{ route('cricket_matches.index') }}">{{__('Match Fixture')}}</a><br><br>
                       <a style="min-width:250px;" class="btn btn-success" href="{{ route('team.points') }}">{{__('Teams Point')}}</a><br><br>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
