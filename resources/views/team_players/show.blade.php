@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Team Player' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('team_players.destroy', $teamPlayer->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('team_players.index') }}" class="btn btn-primary" title="Show All Team Player">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('team_players.create') }}" class="btn btn-success" title="Create New Team Player">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('team_players.edit', $teamPlayer->id ) }}" class="btn btn-primary" title="Edit Team Player">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Team Player" onclick="return confirm(&quot;Click Ok to delete Team Player.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Team</dt>
            <dd>{{ optional($teamPlayer->team)->team_name }}</dd>
            <dt>First Name</dt>
            <dd>{{ $teamPlayer->first_name }}</dd>
            <dt>Last Name</dt>
            <dd>{{ $teamPlayer->last_name }}</dd>
            @php
            $location = '/public/uploads/team_players';
            @endphp
            @if(optional($teamPlayer)->imageUri)
            <dt>Image</dt>
            <dd>{{ $teamPlayer->imageUri }}</dd>
            <dd><img src="{{url($location)}}/{{$teamPlayer->imageUri }}" width="100" height="100"></dd>
            @endif
            <dt>Player Jerseyno</dt>
            <dd>{{ $teamPlayer->player_jerseyno }}</dd>
            <dt>Dob</dt>
            <dd>{{ $teamPlayer->dob }}</dd>
            <dt>Country</dt>
            <dd>{{ $teamPlayer->country }}</dd>
            <dt>Matches</dt>
            <dd>{{ $teamPlayer->matches }}</dd>
            <dt>Run</dt>
            <dd>{{ $teamPlayer->run }}</dd>
            <dt>Highest Scores</dt>
            <dd>{{ $teamPlayer->highest_scores }}</dd>
            <dt>Fifties</dt>
            <dd>{{ $teamPlayer->fifties }}</dd>
            <dt>Hundreds</dt>
            <dd>{{ $teamPlayer->hundreds }}</dd>

        </dl>

    </div>
</div>

@endsection
