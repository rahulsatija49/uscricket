@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            <div class="pull-left">
                <h4 class="mt-5 mb-5">Team Players</h4>
            </div>
        </div>
        <div class="panel-body">
            <!-- customer search form start !-->
            <form action="{{ route('team_players.search') }}" method="post">
              {{ csrf_field() }}
              {{ method_field('POST') }}
                <input id="uuid" type="hidden" class="form-control" name="uuid" value="{{$uuid}}" />

                    <div class="form-group">
                        <label for="customer_location" class="col-md-2 control-label">{{__('Team')}}:</label>
                        <div class="col-md-4">
                            <select class="form-control" id="team_id" name="team_id" required>
                              <option value="">Select team</option>
                              @foreach ($teams as $key => $team)
                              <option value="{{ $key }}" {{ $key==$team_id ? 'selected' : ''}}  >
                                {{ $team }}
                              </option>
                              @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                 <br>
                                <button type="submit" id="submit" class="btn btn-success pull-right">
                                    &#128269; {{ __('Search') }}
                                </button>
                            </div>
                    </div>
            </form>
             <!-- customer search form end !-->

          </div>
        <div class="panel-heading clearfix">
          <div class="btn-group btn-group-sm pull-right" role="group">
              <a href="{{ route('team_players.create') }}" class="btn btn-success" title="Create New Team Player">
                  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
              </a>
          </div>

      </div>
        @if(count($teamPlayers) == 0)
            <div class="panel-body text-center">
                <h4>No Team Players Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
              @php
              $location = '/public/uploads/team_players';
              @endphp
                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Team</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Player type</th>
                            <th>Image Uri</th>
                            <th>Player Jerseyno</th>
                            <th>Dob</th>
                            <th>Country</th>
                            <th>Matches</th>
                            <th>Run</th>
                            <th>Highest Scores</th>
                            <th>Fifties</th>
                            <th>Hundreds</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($teamPlayers as $key=>$teamPlayer)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ optional($teamPlayer->team)->team_name }}</td>
                            <td>{{ $teamPlayer->first_name }}</td>
                            <td>{{ $teamPlayer->last_name }}</td>
                            <td>{{ $teamPlayer->player_type }}</td>
                            <td>{{ $teamPlayer->imageUri }}
                            @if(optional($teamPlayer)->imageUri)
                            <br><img src="{{url($location)}}/{{$teamPlayer->imageUri }}" width="80" height="80"></td>
                            @endif
                            <td>{{ $teamPlayer->player_jerseyno }}</td>
                            <td>{{ $teamPlayer->dob }}</td>
                            <td>{{ $teamPlayer->country }}</td>
                            <td>{{ $teamPlayer->matches }}</td>
                            <td>{{ $teamPlayer->run }}</td>
                            <td>{{ $teamPlayer->highest_scores }}</td>
                            <td>{{ $teamPlayer->fifties }}</td>
                            <td>{{ $teamPlayer->hundreds }}</td>
                            <td>

                                <form method="POST" action="{!! route('team_players.destroy', $teamPlayer->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('team_players.show', $teamPlayer->id ) }}" class="btn btn-info" title="Show Team Player">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('team_players.edit', $teamPlayer->id ) }}" class="btn btn-primary" title="Edit Team Player">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Team Player" onclick="return confirm(&quot;Click Ok to delete Team Player.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $teamPlayers->render() !!}
        </div>

        @endif

    </div>
@endsection
