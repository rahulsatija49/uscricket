
<div class="form-group {{ $errors->has('team_id') ? 'has-error' : '' }}">
    <label for="team_id" class="col-md-2 control-label">Team*</label>
    <div class="col-md-10">
        <select class="form-control" id="team_id" name="team_id" required>
        	<option value="" style="display: none;" {{ old('team_id', optional($teamPlayer)->team_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select team</option>
        	@foreach ($teams as $key => $team)
			    <option value="{{ $key }}" {{ old('team_id', optional($teamPlayer)->team_id) == $key ? 'selected' : '' }}>
			    	{{ $team }}
			    </option>
			@endforeach
        </select>

        {!! $errors->first('team_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
    <div class="col-md-6 {{ $errors->has('first_name') ? 'has-error' : '' }}">
      <label for="first_name" class="col-md-4 form-label-right control-label">First Name*</label>
      <div class="col-md-8">
        <input class="form-control" name="first_name" type="text" id="first_name" value="{{ old('first_name', optional($teamPlayer)->first_name) }}" minlength="1" placeholder="Enter first name here..." required>
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="col-md-6 {{ $errors->has('last_name') ? 'has-error' : '' }}">
      <label for="last_name" class="col-md-4 form-label-right control-label">Last Name*</label>
      <div class="col-md-8">
          <input class="form-control" name="last_name" type="text" id="last_name" value="{{ old('last_name', optional($teamPlayer)->last_name) }}" minlength="1" placeholder="Enter last name here..." required>
          {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>
<div class="form-group ">
    <div class="col-md-6 {{ $errors->has('imageUri') ? 'has-error' : '' }}">
    <label for="imageUri" class="col-md-4 form-label-right control-label">Image</label>
    <div class="col-md-8">
        <input class="form-control" name="imageUri" type="file" id="imageUri" value="{{ old('imageUri', optional($teamPlayer)->imageUri) }}" minlength="1" placeholder="Enter logo uri here...">
        (max file upload size upto 2MB, File type:  "jpeg","png","jpg")
        <br>
          @php
            $location = '/public/uploads/team_players';
          @endphp
          @if(optional($teamPlayer)->imageUri)
            <img src="{{url($location)}}/{{optional($teamPlayer)->imageUri }}" width="200" height="100">
          @endif
        <!-- <input class="form-control" name="imageUri" type="text" id="imageUri" value="{{ old('imageUri', optional($teamPlayer)->imageUri) }}" placeholder="Enter image uri here..."> -->
        {!! $errors->first('imageUri', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
  <div class="col-md-6 {{ $errors->has('player_jerseyno') ? 'has-error' : '' }}">
    <label for="player_jerseyno" class="col-md-4 form-label-right control-label">Player Jerseyno</label>
    <div class="col-md-8">
        <input class="form-control" name="player_jerseyno" type="number" id="player_jerseyno" value="{{ old('player_jerseyno', optional($teamPlayer)->player_jerseyno) }}" minlength="1" placeholder="Enter player jerseyno here...">
        {!! $errors->first('player_jerseyno', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
</div>

<div class="form-group">
  <div class="col-md-6 {{ $errors->has('dob') ? 'has-error' : '' }}">

    <label for="dob" class="col-md-4 form-label-right control-label">Dob</label>
    <div class="col-md-8">
        <input class="form-control" name="dob" type="text" id="dob" value="{{ old('dob', optional($teamPlayer)->dob) }}" minlength="1" placeholder="Enter dob here...">
        {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
    </div>
  </div>

  <div class="col-md-6 {{ $errors->has('country') ? 'has-error' : '' }}">
      <label for="country" class="col-md-4 form-label-right control-label">Country</label>
      <div class="col-md-8">
          <input class="form-control" name="country" type="text" id="country" value="{{ old('country', optional($teamPlayer)->country) }}" placeholder="Enter country here...">
          {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
</div>

<div class="form-group">
  <div class="col-md-6 {{ $errors->has('matches') ? 'has-error' : '' }}">

    <label for="matches" class="col-md-4 form-label-right control-label">Matches</label>
    <div class="col-md-8">
        <input class="form-control" name="matches" type="number" id="matches" value="{{ old('matches', optional($teamPlayer)->matches) }}" minlength="1" placeholder="Enter matches here...">
        {!! $errors->first('matches', '<p class="help-block">:message</p>') !!}
    </div>
  </div>

  <div class="col-md-6 {{ $errors->has('run') ? 'has-error' : '' }}">
      <label for="run" class="col-md-4 form-label-right control-label">Run</label>
      <div class="col-md-8">
          <input class="form-control" name="run" type="number" id="run" value="{{ old('run', optional($teamPlayer)->run) }}" minlength="1" placeholder="Enter run here...">
          {!! $errors->first('run', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
</div>

<div class="form-group">
  <div class="col-md-6 {{ $errors->has('highest_scores') ? 'has-error' : '' }}">

    <label for="highest_scores" class="col-md-4 form-label-right control-label">Highest Scores</label>
    <div class="col-md-8">
        <input class="form-control" name="highest_scores" type="number" id="highest_scores" value="{{ old('highest_scores', optional($teamPlayer)->highest_scores) }}" minlength="1" placeholder="Enter highest scores here...">
        {!! $errors->first('highest_scores', '<p class="help-block">:message</p>') !!}
    </div>
  </div>

  <div class="col-md-6 {{ $errors->has('fifties') ? 'has-error' : '' }}">
      <label for="fifties" class="col-md-4 form-label-right control-label">Fifties</label>
      <div class="col-md-8">
          <input class="form-control" name="fifties" type="number" id="fifties" value="{{ old('fifties', optional($teamPlayer)->fifties) }}" minlength="1" placeholder="Enter fifties here...">
          {!! $errors->first('fifties', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
</div>

<div class="form-group">
  <div class="col-md-6 {{ $errors->has('hundreds') ? 'has-error' : '' }}">

    <label for="hundreds" class="col-md-4 form-label-right control-label">Hundreds</label>
    <div class="col-md-8">
        <input class="form-control" name="hundreds" type="number" id="hundreds" value="{{ old('hundreds', optional($teamPlayer)->hundreds) }}" minlength="1" placeholder="Enter hundreds here...">
        {!! $errors->first('hundreds', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
  <div class="col-md-6 {{ $errors->has('player_type') ? 'has-error' : '' }}">

    <label for="hundreds" class="col-md-4 form-label-right control-label">Player Type*</label>
    <div class="col-md-8">
      <select class="form-control" id="player_type" name="player_type" required>
        <option value="" style="display: none;" {{ old('player_type', optional($teamPlayer)->player_type ?: '') == '' ? 'selected' : '' }} disabled selected>Select Player Type</option>
        <option value="Batsman" {{ old('team_id', optional($teamPlayer)->player_type) == 'Batsman' ? 'selected' : '' }}>Batsman</option>
        <option value="Bowler" {{ old('team_id', optional($teamPlayer)->player_type) == 'Bowler' ? 'selected' : '' }}>Bowler</option>
        <option value="All-rounder" {{ old('team_id', optional($teamPlayer)->player_type) == 'All-rounder' ? 'selected' : '' }}>All rounder</option>
      </select>
        {!! $errors->first('hundreds', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
</div>
<script type="text/javascript">
  jQuery(document).ready(function($) {
       $('#dob').datepicker({
           autoclose: true,
           format: 'yyyy-mm-dd'
        });
     });
</script>
