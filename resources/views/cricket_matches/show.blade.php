@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Cricket Matches' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('cricket_matches.destroy', $cricketMatches->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('cricket_matches.index') }}" class="btn btn-primary" title="Show All Cricket Matches">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('cricket_matches.create') }}" class="btn btn-success" title="Create New Cricket Matches">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('cricket_matches.edit', $cricketMatches->id ) }}" class="btn btn-primary" title="Edit Cricket Matches">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Cricket Matches" onclick="return confirm(&quot;Click Ok to delete Cricket Matches.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Team One</dt>
            <dd>{{ $cricketMatches->team_one }}</dd>
            <dt>Team Two</dt>
            <dd>{{ $cricketMatches->team_two }}</dd>
            <dt>Match Date</dt>
            <dd>{{ $cricketMatches->match_date }}</dd>
            <dt>Match Venue</dt>
            <dd>{{ $cricketMatches->match_venue }}</dd>
            <dt>Match Result</dt>
            <dd>{{ $cricketMatches->match_result }}</dd>

        </dl>

    </div>
</div>

@endsection
