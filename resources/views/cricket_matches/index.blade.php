@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Cricket Matches</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('cricket_matches.create') }}" class="btn btn-success" title="Create New Cricket Matches">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>

        @if(count($cricketMatchesObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Cricket Matches Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Team One</th>
                            <th>Team Two</th>
                            <th>Match Date</th>
                            <th>Match Venue</th>
                            <th>Match Winner</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($cricketMatchesObjects as $cricketMatches)
                        <tr>
                            <td>{{optional($cricketMatches->team_a)->team_name}}</td>
                            <td>{{optional($cricketMatches->team_b)->team_name}}</td>
                            <td>{{ $cricketMatches->match_date }}</td>
                            <td>{{ $cricketMatches->match_venue }}</td>
                            <td>{{ ($cricketMatches->match_result) ? optional($cricketMatches->team_winner)->team_name : 'Pending' }} </td>
                            <td>
                                <form method="POST" action="{!! route('cricket_matches.destroy', $cricketMatches->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}
                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <!-- <a href="{{ route('cricket_matches.show', $cricketMatches->id ) }}" class="btn btn-info" title="Show Cricket Matches">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a> -->
                                        <a href="{{ route('cricket_matches.edit', $cricketMatches->id ) }}" class="btn btn-primary" title="Edit Cricket Matches">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>
                                        <button type="submit" class="btn btn-danger" title="Delete Cricket Matches" onclick="return confirm(&quot;Click Ok to delete Cricket Matches.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer">
            {!! $cricketMatchesObjects->render() !!}
        </div>
        @endif
    </div>
@endsection
