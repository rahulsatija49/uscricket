
<div class="form-group {{ $errors->has('team_one') ? 'has-error' : '' }}">
    <label for="team_one" class="col-md-2 control-label">Team One*</label>
    <div class="col-md-10">
      <select class="form-control" id="team_one" name="team_one" required>
        <option value="" style="display: none;" {{ old('team_one', optional($cricketMatches)->team_one ?: '') == '' ? 'selected' : '' }} disabled selected>Select team</option>
        @foreach ($teams as $key => $team)
        <option value="{{ $key }}" {{ old('team_two', optional($cricketMatches)->team_one) == $key ? 'selected' : '' }}>
          {{ $team }}
        </option>
        @endforeach
      </select>
        <!-- <input class="form-control" name="team_one" type="text" id="team_one" value="{{ old('team_one', optional($cricketMatches)->team_one) }}" minlength="1" placeholder="Enter team one here..."> -->
        {!! $errors->first('team_one', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('team_two') ? 'has-error' : '' }}">
    <label for="team_two" class="col-md-2 control-label">Team Two*</label>
    <div class="col-md-10">
      <select class="form-control" id="team_two" name="team_two" required>
        <option value="" style="display: none;" {{ old('team_two', optional($cricketMatches)->team_two ?: '') == '' ? 'selected' : '' }} disabled selected>Select team</option>
        @foreach ($teams as $key => $team)
        <option value="{{ $key }}" {{ old('team_two', optional($cricketMatches)->team_two) == $key ? 'selected' : '' }}>
          {{ $team }}
        </option>
        @endforeach
      </select>
        <!-- <input class="form-control" name="team_two" type="text" id="team_two" value="{{ old('team_two', optional($cricketMatches)->team_two) }}" minlength="1" placeholder="Enter team two here..."> -->
        {!! $errors->first('team_two', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('match_date') ? 'has-error' : '' }}">
    <label for="match_date" class="col-md-2 control-label">Match Date</label>
    <div class="col-md-10">
        <input class="form-control" name="match_date" type="text" id="match_date" value="{{ old('match_date', optional($cricketMatches)->match_date) }}" placeholder="Enter match date here...">
        {!! $errors->first('match_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('match_venue') ? 'has-error' : '' }}">
    <label for="match_venue" class="col-md-2 control-label">Match Venue</label>
    <div class="col-md-10">
        <input class="form-control" name="match_venue" type="text" id="match_venue" value="{{ old('match_venue', optional($cricketMatches)->match_venue) }}" minlength="1" placeholder="Enter match venue here...">
        {!! $errors->first('match_venue', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<script type="text/javascript">
  jQuery(document).ready(function($) {
       $('#match_date').datepicker({
           autoclose: true,
           format: 'yyyy-mm-dd'
        });
     });
</script>

  @if(optional($cricketMatches)->id)
  <h4 style="color:green"><b>Select Winner team</b> </h4>
   <div class="form-group {{ $errors->has('match_result') ? 'has-error' : '' }}">
    <label for="match_result" class="col-md-2 control-label">Match Result</label>
    <div class="col-md-10">
      <select class="form-control" id="match_result" name="match_result" required>
        <option value="" style="display: none;" {{ old('match_result', optional($cricketMatches)->match_result ?: '') == '' ? 'selected' : '' }} disabled selected>Select winner team</option>
        @foreach ($teams as $key => $team)
          @if(optional($cricketMatches)->team_one ==  $key || optional($cricketMatches)->team_two ==  $key)
          <option value="{{ $key }}" {{ old('match_result', optional($cricketMatches)->match_result) == $key ? 'selected' : '' }}>
            {{ $team }}
          </option>
          @endif
        @endforeach
      </select>
        <!-- <input class="form-control" name="match_result" type="text" id="match_result" value="{{ old('match_result', optional($cricketMatches)->match_result) }}" minlength="1" placeholder="Enter match result here..."> -->
        {!! $errors->first('match_result', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
  @endif
