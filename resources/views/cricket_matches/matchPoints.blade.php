@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <div class="pull-left">
                <h4 class="mt-5 mb-5">Cricket Matches Points</h4>
            </div>
        </div>
        @if(count($team_data) == 0)
            <div class="panel-body text-center">
                <h4>No Team Points Available.</h4>
            </div>
        @else

        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Teams</th>
                            <th>Played</th>
                            <th>Win</th>
                            <th>Loose</th>
                            <th>No result</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($team_data as $team)
            
                        <tr>
                            <td>{{ $team['name'] }}</td>
                            <td>{{ $team['team_matches'] }}</td>
                            <td>{{ $team['team_win_count'] }}</td>
                            <td>{{ $team['team_loose_count'] }}</td>
                            <td>{{ $team['team_no_result'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer">

        </div>
        @endif
    </div>
@endsection
