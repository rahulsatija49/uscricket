
<div class="form-group {{ $errors->has('team_name') ? 'has-error' : '' }}">
    <label for="team_name" class="col-md-2 control-label">Team Name*</label>
    <div class="col-md-10">
        <input class="form-control" name="team_name" type="text" id="team_name" value="{{ old('team_name', optional($team)->team_name) }}" minlength="1" placeholder="Enter team name here..." required>
        {!! $errors->first('team_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('logoUri') ? 'has-error' : '' }}">
    <label for="logoUri" class="col-md-2 control-label">Logo</label>
    <div class="col-md-10">
        <input class="form-control" name="logoUri" type="file" id="logoUri" value="{{ old('logoUri', optional($team)->logoUri) }}" minlength="1" placeholder="Enter logo uri here...">
        (max file upload size upto 2MB, File type:  "jpeg","png","jpg")
        <br>
        @php
        $location = '/public/uploads/team_logo';
        @endphp
        @if(optional($team)->logoUri)
        <img src="{{url($location)}}/{{optional($team)->logoUri }}" width="200" height="100">
        @endif

        {!! $errors->first('logoUri', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('team_clubstate') ? 'has-error' : '' }}">
    <label for="team_clubstate" class="col-md-2 control-label">Club state*</label>
    <div class="col-md-10">
        <input class="form-control" name="team_clubstate" type="text" id="team_clubstate" value="{{ old('team_clubstate', optional($team)->team_clubstate) }}" minlength="1" placeholder="Enter team clubstate here..." required>
        {!! $errors->first('team_clubstate', '<p class="help-block">:message</p>') !!}
    </div>
</div>
