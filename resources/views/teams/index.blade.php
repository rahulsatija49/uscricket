@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Teams</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('teams.create') }}" class="btn btn-success" title="Create New Team">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>

        @if(count($teams) == 0)
            <div class="panel-body text-center">
                <h4>No Teams Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
              @php
              $location = '/public/uploads/team_logo';
              @endphp
                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Team Name</th>
                            <th>Logo Uri</th>
                            <th>Club state</th>
                            <th>Team Players</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($teams as $key=>$team)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $team->team_name }}</td>
                            <td>{{ $team->logoUri }}
                            @if(optional($team)->logoUri)
                            <br>  <img src="{{url($location)}}/{{$team->logoUri }}" width="50" height="50">
                            @endif

                            </td>

                            <td>{{ $team->team_clubstate }}</td>
                            <td><a href="{{ route('teams.show', $team->id ) }}">View All</a></td>

                            <td>

                                <form method="POST" action="{!! route('teams.destroy', $team->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('teams.show', $team->id ) }}" class="btn btn-info" title="Show Team">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('teams.edit', $team->id ) }}" class="btn btn-primary" title="Edit Team">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Team" onclick="return confirm(&quot;Click Ok to delete Team.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $teams->render() !!}
        </div>

        @endif

    </div>
@endsection
