@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Team' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('teams.destroy', $team->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('teams.index') }}" class="btn btn-primary" title="Show All Team">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('teams.create') }}" class="btn btn-success" title="Create New Team">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('teams.edit', $team->id ) }}" class="btn btn-primary" title="Edit Team">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Team" onclick="return confirm(&quot;Click Ok to delete Team.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>


    </div>

    @php
    $location = '/public/uploads/team_logo';
    @endphp
    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Team Name</dt>
            <dd>{{ $team->team_name }}</dd>
            @if(optional($team)->logoUri)
            <dt>Logo</dt>
            <dd>{{ $team->logoUri }}</dd>
            <dd><img src="{{url($location)}}/{{$team->logoUri }}" width="100" height="100"></dd>
            @endif
            <dt>Club state</dt>
            <dd>{{ $team->team_clubstate }}</dd>

        </dl>

    </div>

    <div class="panel-heading clearfix">
      <div class="btn-group btn-group-sm pull-right" role="group">
          <a href="{{ route('team_players.create') }}" class="btn btn-success" title="Create New Team Player">
              <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
          </a>
      </div>
    </div>
    @if(count($teamPlayers) == 0)
        <div class="panel-body text-center">
            <h4>No Team Players Available.</h4>
        </div>
    @else
    <div class="panel-body panel-body-with-table">
        <div class="table-responsive">
          @php
          $location = '/public/uploads/team_players';
          @endphp
            <table class="table table-striped ">
                <thead>
                    <tr>
                        <th>Team</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Image Uri</th>
                        <th>Player Jerseyno</th>
                        <th>Dob</th>
                        <th>Country</th>
                        <th>Matches</th>
                        <th>Run</th>
                        <th>Highest Scores</th>
                        <th>Fifties</th>
                        <th>Hundreds</th>
                        <th>Is Active</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($teamPlayers as $teamPlayer)
                    <tr>
                        <td>{{ optional($teamPlayer->team)->team_name }}</td>
                        <td>{{ $teamPlayer->first_name }}</td>
                        <td>{{ $teamPlayer->last_name }}</td>
                        <td>{{ $teamPlayer->imageUri }}<br>
                        <img src="{{url($location)}}/{{$teamPlayer->imageUri }}" width="80" height="80"></td>
                        <td>{{ $teamPlayer->player_jerseyno }}</td>
                        <td>{{ $teamPlayer->dob }}</td>
                        <td>{{ $teamPlayer->country }}</td>
                        <td>{{ $teamPlayer->matches }}</td>
                        <td>{{ $teamPlayer->run }}</td>
                        <td>{{ $teamPlayer->highest_scores }}</td>
                        <td>{{ $teamPlayer->fifties }}</td>
                        <td>{{ $teamPlayer->hundreds }}</td>
                        <td>{{ ($teamPlayer->is_active) ? 'Yes' : 'No' }}</td>

                        <td>
                                <div class="btn-group btn-group-xs pull-right" role="group">
                                    <a href="{{ route('team_players.show', $teamPlayer->id ) }}" class="btn btn-info" title="Show Team Player">
                                        <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                    </a>
                                </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

    <div class="panel-footer">
        {!! $teamPlayers->render() !!}
    </div>

    @endif
</div>

@endsection
